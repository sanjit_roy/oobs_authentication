import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:oobs_authentication/oobs_authentication.dart';

class IdentityDto {
  /// Builds an [Identity] type from a [GoogleSignInAccount?]

  /// Builds an [Identity] from a [GoogleUserCredential]
  static Option<Identity> fromGoogleUserCredential(UserCredential credential) {
    final User? user = credential.user;
    if (user == null) {
      return const None<Identity>();
    }

    return Some<Identity>(Identity(
        emailId: user.email,
        uid: user.uid,
        username: user.displayName,
        profilePicUrl: user.photoURL));
  }

  /// Builds an [Identity] from a Firebase user.
  /// Used with authenticated google profile when fromGoogleAccount does not work.
  static Option<Identity> fromFirebaseUser(User? firebaseUser) =>
      (firebaseUser == null)
          ? const None<Identity>()
          : Some<Identity>(Identity(
              uid: firebaseUser.uid,
              emailId: firebaseUser.email,
              profilePicUrl: firebaseUser.photoURL,
              username: firebaseUser.displayName,
            ));

  // TODO(roy): Checks are not inplace.
  static Identity fromJson(dynamic json) => Identity(
      uid: json['id'].toString(),
      emailId: json['email'].toString(),
      profilePicUrl: json['photoUrl'].toString(),
      username: json['displayName'].toString());

  static Map<String, dynamic> toJson(Identity id) => id.value.fold(
      (TypeFailure l) => <String, dynamic>{'error': l.toString()},
      (dynamic r) => <String, dynamic>{
            'displayName': r.displayName,
            'email': r.email,
            'photoUrl': r.photoUrl.fold(() => '', (String a) => a)
          });
}

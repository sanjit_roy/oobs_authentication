import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:oobs_authentication/oobs_authentication.dart';

import '../../utils.dart';
part 'identity.type.freezed.dart';

class Identity {
  factory Identity(
      {required String? uid,
      required String? emailId,
      required String? username,
      required String? profilePicUrl}) {
    return Util.checkNullOrEmptyString(uid).fold(
        () => Identity._(left(const TypeFailure.mustHaveUid())),
        (String uid) => Util.checkNullOrEmptyString(emailId).fold(
            () => Identity._(left(const TypeFailure.mustHaveEmail())),
            (String email) => Util.checkNullOrEmptyString(username).fold(
                () => Identity._(left(const TypeFailure.mustHaveDisplayName())),
                (String name) => Identity._(right(IdentityRecord(
                    uid: uid,
                    emailId: email,
                    userName: name,
                    profilePicUrl:
                        Util.checkNullOrEmptyString(profilePicUrl)))))));
  }

  const Identity._(this.value);
  final Either<TypeFailure, IdentityRecord> value;
}

@freezed
class IdentityRecord with _$IdentityRecord {
  const factory IdentityRecord(
      {required String uid,
      required String emailId,
      required String userName,
      required Option<String> profilePicUrl}) = _IdentityRecord;
}

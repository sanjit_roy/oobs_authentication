// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'identity.type.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$IdentityRecordTearOff {
  const _$IdentityRecordTearOff();

  _IdentityRecord call(
      {required String uid,
      required String emailId,
      required String userName,
      required Option<String> profilePicUrl}) {
    return _IdentityRecord(
      uid: uid,
      emailId: emailId,
      userName: userName,
      profilePicUrl: profilePicUrl,
    );
  }
}

/// @nodoc
const $IdentityRecord = _$IdentityRecordTearOff();

/// @nodoc
mixin _$IdentityRecord {
  String get uid => throw _privateConstructorUsedError;
  String get emailId => throw _privateConstructorUsedError;
  String get userName => throw _privateConstructorUsedError;
  Option<String> get profilePicUrl => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $IdentityRecordCopyWith<IdentityRecord> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $IdentityRecordCopyWith<$Res> {
  factory $IdentityRecordCopyWith(
          IdentityRecord value, $Res Function(IdentityRecord) then) =
      _$IdentityRecordCopyWithImpl<$Res>;
  $Res call(
      {String uid,
      String emailId,
      String userName,
      Option<String> profilePicUrl});
}

/// @nodoc
class _$IdentityRecordCopyWithImpl<$Res>
    implements $IdentityRecordCopyWith<$Res> {
  _$IdentityRecordCopyWithImpl(this._value, this._then);

  final IdentityRecord _value;
  // ignore: unused_field
  final $Res Function(IdentityRecord) _then;

  @override
  $Res call({
    Object? uid = freezed,
    Object? emailId = freezed,
    Object? userName = freezed,
    Object? profilePicUrl = freezed,
  }) {
    return _then(_value.copyWith(
      uid: uid == freezed
          ? _value.uid
          : uid // ignore: cast_nullable_to_non_nullable
              as String,
      emailId: emailId == freezed
          ? _value.emailId
          : emailId // ignore: cast_nullable_to_non_nullable
              as String,
      userName: userName == freezed
          ? _value.userName
          : userName // ignore: cast_nullable_to_non_nullable
              as String,
      profilePicUrl: profilePicUrl == freezed
          ? _value.profilePicUrl
          : profilePicUrl // ignore: cast_nullable_to_non_nullable
              as Option<String>,
    ));
  }
}

/// @nodoc
abstract class _$IdentityRecordCopyWith<$Res>
    implements $IdentityRecordCopyWith<$Res> {
  factory _$IdentityRecordCopyWith(
          _IdentityRecord value, $Res Function(_IdentityRecord) then) =
      __$IdentityRecordCopyWithImpl<$Res>;
  @override
  $Res call(
      {String uid,
      String emailId,
      String userName,
      Option<String> profilePicUrl});
}

/// @nodoc
class __$IdentityRecordCopyWithImpl<$Res>
    extends _$IdentityRecordCopyWithImpl<$Res>
    implements _$IdentityRecordCopyWith<$Res> {
  __$IdentityRecordCopyWithImpl(
      _IdentityRecord _value, $Res Function(_IdentityRecord) _then)
      : super(_value, (v) => _then(v as _IdentityRecord));

  @override
  _IdentityRecord get _value => super._value as _IdentityRecord;

  @override
  $Res call({
    Object? uid = freezed,
    Object? emailId = freezed,
    Object? userName = freezed,
    Object? profilePicUrl = freezed,
  }) {
    return _then(_IdentityRecord(
      uid: uid == freezed
          ? _value.uid
          : uid // ignore: cast_nullable_to_non_nullable
              as String,
      emailId: emailId == freezed
          ? _value.emailId
          : emailId // ignore: cast_nullable_to_non_nullable
              as String,
      userName: userName == freezed
          ? _value.userName
          : userName // ignore: cast_nullable_to_non_nullable
              as String,
      profilePicUrl: profilePicUrl == freezed
          ? _value.profilePicUrl
          : profilePicUrl // ignore: cast_nullable_to_non_nullable
              as Option<String>,
    ));
  }
}

/// @nodoc
class _$_IdentityRecord implements _IdentityRecord {
  const _$_IdentityRecord(
      {required this.uid,
      required this.emailId,
      required this.userName,
      required this.profilePicUrl});

  @override
  final String uid;
  @override
  final String emailId;
  @override
  final String userName;
  @override
  final Option<String> profilePicUrl;

  @override
  String toString() {
    return 'IdentityRecord(uid: $uid, emailId: $emailId, userName: $userName, profilePicUrl: $profilePicUrl)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _IdentityRecord &&
            (identical(other.uid, uid) ||
                const DeepCollectionEquality().equals(other.uid, uid)) &&
            (identical(other.emailId, emailId) ||
                const DeepCollectionEquality()
                    .equals(other.emailId, emailId)) &&
            (identical(other.userName, userName) ||
                const DeepCollectionEquality()
                    .equals(other.userName, userName)) &&
            (identical(other.profilePicUrl, profilePicUrl) ||
                const DeepCollectionEquality()
                    .equals(other.profilePicUrl, profilePicUrl)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(uid) ^
      const DeepCollectionEquality().hash(emailId) ^
      const DeepCollectionEquality().hash(userName) ^
      const DeepCollectionEquality().hash(profilePicUrl);

  @JsonKey(ignore: true)
  @override
  _$IdentityRecordCopyWith<_IdentityRecord> get copyWith =>
      __$IdentityRecordCopyWithImpl<_IdentityRecord>(this, _$identity);
}

abstract class _IdentityRecord implements IdentityRecord {
  const factory _IdentityRecord(
      {required String uid,
      required String emailId,
      required String userName,
      required Option<String> profilePicUrl}) = _$_IdentityRecord;

  @override
  String get uid => throw _privateConstructorUsedError;
  @override
  String get emailId => throw _privateConstructorUsedError;
  @override
  String get userName => throw _privateConstructorUsedError;
  @override
  Option<String> get profilePicUrl => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$IdentityRecordCopyWith<_IdentityRecord> get copyWith =>
      throw _privateConstructorUsedError;
}

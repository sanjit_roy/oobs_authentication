import 'package:freezed_annotation/freezed_annotation.dart';

part 'type_failure.freezed.dart';

@freezed
class TypeFailure with _$TypeFailure {
  const factory TypeFailure.mustHaveUid() = MustHaveUid;
  const factory TypeFailure.mustHaveEmail() = MustHaveEmail;
  const factory TypeFailure.mustHaveDisplayName() = MustHaveDisplayName;
}

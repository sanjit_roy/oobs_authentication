// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'type_failure.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$TypeFailureTearOff {
  const _$TypeFailureTearOff();

  MustHaveUid mustHaveUid() {
    return const MustHaveUid();
  }

  MustHaveEmail mustHaveEmail() {
    return const MustHaveEmail();
  }

  MustHaveDisplayName mustHaveDisplayName() {
    return const MustHaveDisplayName();
  }
}

/// @nodoc
const $TypeFailure = _$TypeFailureTearOff();

/// @nodoc
mixin _$TypeFailure {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() mustHaveUid,
    required TResult Function() mustHaveEmail,
    required TResult Function() mustHaveDisplayName,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? mustHaveUid,
    TResult Function()? mustHaveEmail,
    TResult Function()? mustHaveDisplayName,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(MustHaveUid value) mustHaveUid,
    required TResult Function(MustHaveEmail value) mustHaveEmail,
    required TResult Function(MustHaveDisplayName value) mustHaveDisplayName,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(MustHaveUid value)? mustHaveUid,
    TResult Function(MustHaveEmail value)? mustHaveEmail,
    TResult Function(MustHaveDisplayName value)? mustHaveDisplayName,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TypeFailureCopyWith<$Res> {
  factory $TypeFailureCopyWith(
          TypeFailure value, $Res Function(TypeFailure) then) =
      _$TypeFailureCopyWithImpl<$Res>;
}

/// @nodoc
class _$TypeFailureCopyWithImpl<$Res> implements $TypeFailureCopyWith<$Res> {
  _$TypeFailureCopyWithImpl(this._value, this._then);

  final TypeFailure _value;
  // ignore: unused_field
  final $Res Function(TypeFailure) _then;
}

/// @nodoc
abstract class $MustHaveUidCopyWith<$Res> {
  factory $MustHaveUidCopyWith(
          MustHaveUid value, $Res Function(MustHaveUid) then) =
      _$MustHaveUidCopyWithImpl<$Res>;
}

/// @nodoc
class _$MustHaveUidCopyWithImpl<$Res> extends _$TypeFailureCopyWithImpl<$Res>
    implements $MustHaveUidCopyWith<$Res> {
  _$MustHaveUidCopyWithImpl(
      MustHaveUid _value, $Res Function(MustHaveUid) _then)
      : super(_value, (v) => _then(v as MustHaveUid));

  @override
  MustHaveUid get _value => super._value as MustHaveUid;
}

/// @nodoc
class _$MustHaveUid implements MustHaveUid {
  const _$MustHaveUid();

  @override
  String toString() {
    return 'TypeFailure.mustHaveUid()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is MustHaveUid);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() mustHaveUid,
    required TResult Function() mustHaveEmail,
    required TResult Function() mustHaveDisplayName,
  }) {
    return mustHaveUid();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? mustHaveUid,
    TResult Function()? mustHaveEmail,
    TResult Function()? mustHaveDisplayName,
    required TResult orElse(),
  }) {
    if (mustHaveUid != null) {
      return mustHaveUid();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(MustHaveUid value) mustHaveUid,
    required TResult Function(MustHaveEmail value) mustHaveEmail,
    required TResult Function(MustHaveDisplayName value) mustHaveDisplayName,
  }) {
    return mustHaveUid(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(MustHaveUid value)? mustHaveUid,
    TResult Function(MustHaveEmail value)? mustHaveEmail,
    TResult Function(MustHaveDisplayName value)? mustHaveDisplayName,
    required TResult orElse(),
  }) {
    if (mustHaveUid != null) {
      return mustHaveUid(this);
    }
    return orElse();
  }
}

abstract class MustHaveUid implements TypeFailure {
  const factory MustHaveUid() = _$MustHaveUid;
}

/// @nodoc
abstract class $MustHaveEmailCopyWith<$Res> {
  factory $MustHaveEmailCopyWith(
          MustHaveEmail value, $Res Function(MustHaveEmail) then) =
      _$MustHaveEmailCopyWithImpl<$Res>;
}

/// @nodoc
class _$MustHaveEmailCopyWithImpl<$Res> extends _$TypeFailureCopyWithImpl<$Res>
    implements $MustHaveEmailCopyWith<$Res> {
  _$MustHaveEmailCopyWithImpl(
      MustHaveEmail _value, $Res Function(MustHaveEmail) _then)
      : super(_value, (v) => _then(v as MustHaveEmail));

  @override
  MustHaveEmail get _value => super._value as MustHaveEmail;
}

/// @nodoc
class _$MustHaveEmail implements MustHaveEmail {
  const _$MustHaveEmail();

  @override
  String toString() {
    return 'TypeFailure.mustHaveEmail()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is MustHaveEmail);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() mustHaveUid,
    required TResult Function() mustHaveEmail,
    required TResult Function() mustHaveDisplayName,
  }) {
    return mustHaveEmail();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? mustHaveUid,
    TResult Function()? mustHaveEmail,
    TResult Function()? mustHaveDisplayName,
    required TResult orElse(),
  }) {
    if (mustHaveEmail != null) {
      return mustHaveEmail();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(MustHaveUid value) mustHaveUid,
    required TResult Function(MustHaveEmail value) mustHaveEmail,
    required TResult Function(MustHaveDisplayName value) mustHaveDisplayName,
  }) {
    return mustHaveEmail(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(MustHaveUid value)? mustHaveUid,
    TResult Function(MustHaveEmail value)? mustHaveEmail,
    TResult Function(MustHaveDisplayName value)? mustHaveDisplayName,
    required TResult orElse(),
  }) {
    if (mustHaveEmail != null) {
      return mustHaveEmail(this);
    }
    return orElse();
  }
}

abstract class MustHaveEmail implements TypeFailure {
  const factory MustHaveEmail() = _$MustHaveEmail;
}

/// @nodoc
abstract class $MustHaveDisplayNameCopyWith<$Res> {
  factory $MustHaveDisplayNameCopyWith(
          MustHaveDisplayName value, $Res Function(MustHaveDisplayName) then) =
      _$MustHaveDisplayNameCopyWithImpl<$Res>;
}

/// @nodoc
class _$MustHaveDisplayNameCopyWithImpl<$Res>
    extends _$TypeFailureCopyWithImpl<$Res>
    implements $MustHaveDisplayNameCopyWith<$Res> {
  _$MustHaveDisplayNameCopyWithImpl(
      MustHaveDisplayName _value, $Res Function(MustHaveDisplayName) _then)
      : super(_value, (v) => _then(v as MustHaveDisplayName));

  @override
  MustHaveDisplayName get _value => super._value as MustHaveDisplayName;
}

/// @nodoc
class _$MustHaveDisplayName implements MustHaveDisplayName {
  const _$MustHaveDisplayName();

  @override
  String toString() {
    return 'TypeFailure.mustHaveDisplayName()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is MustHaveDisplayName);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() mustHaveUid,
    required TResult Function() mustHaveEmail,
    required TResult Function() mustHaveDisplayName,
  }) {
    return mustHaveDisplayName();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? mustHaveUid,
    TResult Function()? mustHaveEmail,
    TResult Function()? mustHaveDisplayName,
    required TResult orElse(),
  }) {
    if (mustHaveDisplayName != null) {
      return mustHaveDisplayName();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(MustHaveUid value) mustHaveUid,
    required TResult Function(MustHaveEmail value) mustHaveEmail,
    required TResult Function(MustHaveDisplayName value) mustHaveDisplayName,
  }) {
    return mustHaveDisplayName(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(MustHaveUid value)? mustHaveUid,
    TResult Function(MustHaveEmail value)? mustHaveEmail,
    TResult Function(MustHaveDisplayName value)? mustHaveDisplayName,
    required TResult orElse(),
  }) {
    if (mustHaveDisplayName != null) {
      return mustHaveDisplayName(this);
    }
    return orElse();
  }
}

abstract class MustHaveDisplayName implements TypeFailure {
  const factory MustHaveDisplayName() = _$MustHaveDisplayName;
}

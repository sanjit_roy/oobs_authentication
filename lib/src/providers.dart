import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

/// Provides a reference to FirebaseAuth.
final Provider<FirebaseAuth> firebaseAuthProvider =
    Provider<FirebaseAuth>((ProviderReference ref) => FirebaseAuth.instance);

/// Provides a reference to GoogleSignIn.
final Provider<GoogleSignIn> googleSignInProvider = Provider<GoogleSignIn>(
    (ProviderReference ref) => GoogleSignIn(scopes: <String>['email']));

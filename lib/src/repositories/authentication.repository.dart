import 'package:dartz/dartz.dart';
import 'package:oobs_authentication/oobs_authentication.dart';

abstract class AuthenticationRepository {
  Future<Option<Identity>> signIn();
  Option<Identity> getUser();
  Future<void> signOut();
  Option<Future<String>> getTokenId();
}

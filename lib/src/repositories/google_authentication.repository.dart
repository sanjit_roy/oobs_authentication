import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:oobs_authentication/oobs_authentication.dart';

import '../providers.dart';
import 'authentication.repository.dart';

final Provider<AuthenticationRepository>
    googleAuthenticationRepositoryProvider = Provider<AuthenticationRepository>(
        (ProviderReference ref) => GoogleAuthenticationRepository(ref.read));

class GoogleAuthenticationRepository implements AuthenticationRepository {
  const GoogleAuthenticationRepository(this._reader);
  final Reader _reader;

  @override
  Option<Identity> getUser() =>
      IdentityDto.fromFirebaseUser(_reader(firebaseAuthProvider).currentUser);

  /// Authenticates a user using Firebase Authentication and builds a Identity type.
  @override
  Future<Option<Identity>> signIn() async {
    try {
      final GoogleSignInAccount? googleUser =
          await _reader(googleSignInProvider).signIn();
      final GoogleSignInAuthentication googleAuth =
          // BUG(roy): Unhandled exception: Null check operator used on a null value.
          // Replication: Press back button on the authentication modal that google opens for signing in.
          await googleUser!.authentication;
      return IdentityDto.fromGoogleUserCredential(
          await _reader(firebaseAuthProvider).signInWithCredential(
              GoogleAuthProvider.credential(
                  idToken: googleAuth.idToken,
                  accessToken: googleAuth.accessToken)));
    } on FirebaseAuthException catch (error) {
      // BUG(roy): Unhandled Exception: [firebase_auth/invalid-credential] null
      // Replication: Unknown
      throw FirebaseAuthException(code: error.code);
    }
  }

  @override
  Future<void> signOut() async {
    await _reader(firebaseAuthProvider).signOut();
  }

  Option<Future<String>> getTokenId() {
    final User? user = _reader(firebaseAuthProvider).currentUser;
    if (user != null) {
      return Some(user.getIdToken());
    } else {
      return None();
    }
  }
}

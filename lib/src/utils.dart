import 'package:dartz/dartz.dart';

class Util {
  /// Checks for null or empty strings,
  /// returns None if null or empty.
  static Option<String> checkNullOrEmptyString(String? value) {
    if (value == null || value.isEmpty) {
      return const None<String>();
    } else {
      return Some<String>(value);
    }
  }
}

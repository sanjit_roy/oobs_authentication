import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:oobs_authentication/oobs_authentication.dart';

void main() {
  group('user type:', () {
    test('user should have a uid', () {
      expect(validUserFixture.value.fold((l) => l, (r) => r.uid), uid);
      expect(userWithNoUidFixture.value.fold((l) => l, (r) => r),
          TypeFailure.mustHaveUid());
      expect(userWithNullUidFixture.value.fold((l) => l, (r) => r),
          TypeFailure.mustHaveUid());
    });
    test('user should have a email id', () {
      expect(validUserFixture.value.fold((l) => l, (r) => r.emailId), emailId);
      expect(userWithNoEmailFixture.value.fold((l) => l, (r) => r),
          TypeFailure.mustHaveEmail());
      expect(userWithNullEmailFixture.value.fold((l) => l, (r) => r),
          TypeFailure.mustHaveEmail());
    });
    test('User might have a profile picture', () {
      expect(
          validUserFixture.value.fold(
              (l) => l, (r) => r.profilePicUrl.fold(() => null, (a) => a)),
          profilePicUrl);
      expect(
          userWithNoProfilePictureFixture.value
              .fold((l) => l, (r) => r.profilePicUrl),
          None<dynamic>());
      expect(
          userWithNullProfilePictureFixture.value
              .fold((l) => l, (r) => r.profilePicUrl),
          None<dynamic>());
    });
    test('User should have a display name', () {
      expect(
          validUserFixture.value.fold((l) => l, (r) => r.userName), username);
      expect(userWithNoDisplayNameFixture.value.fold((l) => l, (r) => r),
          TypeFailure.mustHaveDisplayName());
      expect(userWithNullDisplayNameFixture.value.fold((l) => l, (r) => r),
          TypeFailure.mustHaveDisplayName());
    });
  });
}

final String uid = 'uid';
final String emailId = 'emailId';
final String username = 'username';
final String profilePicUrl = 'url';

final Identity validUserFixture = Identity(
    uid: uid,
    emailId: emailId,
    username: username,
    profilePicUrl: profilePicUrl);

final Identity userWithNoUidFixture = Identity(
    uid: '',
    emailId: emailId,
    username: username,
    profilePicUrl: profilePicUrl);

final Identity userWithNullUidFixture = Identity(
    uid: null,
    emailId: emailId,
    username: username,
    profilePicUrl: profilePicUrl);

final Identity userWithNoEmailFixture = Identity(
    uid: uid, emailId: '', username: username, profilePicUrl: profilePicUrl);

final Identity userWithNullEmailFixture = Identity(
    uid: uid, emailId: null, username: username, profilePicUrl: profilePicUrl);

final Identity userWithNoDisplayNameFixture = Identity(
    uid: uid, emailId: emailId, username: '', profilePicUrl: profilePicUrl);

final Identity userWithNullDisplayNameFixture = Identity(
    uid: uid, emailId: emailId, username: null, profilePicUrl: profilePicUrl);

final Identity userWithNoProfilePictureFixture =
    Identity(uid: uid, emailId: emailId, username: username, profilePicUrl: '');

final Identity userWithNullProfilePictureFixture = Identity(
    uid: uid, emailId: emailId, username: username, profilePicUrl: null);
